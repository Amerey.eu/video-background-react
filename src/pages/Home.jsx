import React from "react";
import videoBg from "../assets/videoBg.mp4";

function Home() {
  return (
    <div className="main">
      <div className="overlay"></div>
      <video src={videoBg} autoPlay loop muted />
      <div className="content">
        <h1>Future</h1>
        <p>is here</p>
      </div>
    </div>
  );
}

export default Home;

